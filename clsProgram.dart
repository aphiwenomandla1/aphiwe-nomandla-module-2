import 'dart:ffi';

class Winner {
  var name = "Crossover";
  var category = "Finance";
  var yearWon = 2022;
  var developer = "Aphiwe Nomandla";
}

void main() {
  String nme = Winner().name;
  String ctgry = Winner().category;
  int yrwon = Winner().yearWon;
  String dvlpr = Winner().developer;

  print(
      "App Name: $nme, App Category: $ctgry, Year Won: $yrwon, Developer: $dvlpr");
}
